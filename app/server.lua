local function toggle_led()
  s = gpio.read(7)
  if s == 1 then
    gpio.write(7, 0)
  else
    gpio.write(7, 1)
  end
end

local server = function(conn)
  local send_tmr = tmr.create()

  local function send_adc()
    data = "ADC:" .. adc.read(0) .. "\n"
    conn:send(data)
    toggle_led()
  end

  local function adc_callback(t)
    if pcall(send_adc) then
      t:start()
    else
      print("Error sending")
      t:unregister()
    end
  end

  local function disconnect(sck, err)
    print("Disconnected")
    send_tmr:unregister()
    gpio.write(7, 0)
  end

  send_tmr:register(500, tmr.ALARM_SEMI, adc_callback)
  send_tmr:start()
  conn:on("disconnection", disconnect)
end

gpio.mode(7, gpio.OUTPUT)

sv = net.createServer(net.TCP, 30)
if sv then
  sv:listen(1234, server)
end
